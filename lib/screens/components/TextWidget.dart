import 'package:flutter/cupertino.dart';

class TextWidget extends StatelessWidget {
  final String text;

  TextWidget(
    this.text, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      "$text",
      style: TextStyle(
        fontSize: 15.0,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
