import 'dart:convert';

import 'package:FlagApp/model/Country.dart';
import 'package:FlagApp/widgets/CountryCardWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  List<Country> countries = List();

  var isLoading = false;

  Future _fetchData() async {
    setState(() {
      isLoading = true;
    });

    String url = "https://restcountries.eu/rest/v1/all";
    final response = await http.get(url);

    if (response.statusCode == 200) {
      countries = (json.decode(response.body) as List)
          .map((data) => new Country.fromJson(data))
          .toList();
    }

    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    _fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Countries List"),
        ),
        drawer: Drawer(
          child: Scaffold(),
        ),
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: countries.length,
                // separatorBuilder: (BuildContext context, int index) =>
                //     Divider(),
                itemBuilder: (BuildContext context, int index) {
                  return CountryCardWidget(
                    country: countries[index],
                  );
                },
              ));
  }
}
