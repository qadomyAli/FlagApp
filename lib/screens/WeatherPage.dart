import 'package:FlagApp/model/Country.dart';
import 'package:FlagApp/widgets/TodayCard.dart';
import 'package:FlagApp/widgets/TomorrowCard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'components/TextWidget.dart';

class WeatherPage extends StatelessWidget {
  final Country country;

  WeatherPage({@required this.country});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${country.name} Weather"),
      ),
      body: Container(
        child: Column(
          children: [
            Container(
              height: 200.0,
              color: Colors.grey.shade300,
              child: Padding(
                padding: EdgeInsets.only(top: 30, left: 30),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: FadeInImage.assetNetwork(
                        width: 120.0,
                        height: 80.0,
                        placeholder: "assets/images/no_data.png",
                        image:
                            "http://www.geognos.com/api/en/countries/flag/${country.alpha2Code}.png",
                        fit: BoxFit.cover,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20),
                      child: Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                TextWidget("Name"),
                                SizedBox(width: 25.0),
                                Text(country.name)
                              ],
                            ),
                            Row(
                              children: [
                                TextWidget("Region"),
                                SizedBox(width: 25.0),
                                Text(country.region.toString())
                              ],
                            ),
                            Row(
                              children: [
                                TextWidget("Capital"),
                                SizedBox(width: 25.0),
                                Text(country.capital)
                              ],
                            ),
                            Row(
                              children: [
                                TextWidget("Population"),
                                SizedBox(width: 25.0),
                                Text(country.population.toString())
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              child: DefaultTabController(
                length: 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TabBar(
                      unselectedLabelColor: Colors.grey,
                      labelColor: Colors.blueAccent,
                      labelStyle: TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                      ),
                      indicatorColor: Colors.blueAccent,
                      tabs: <Widget>[
                        Tab(text: "Today"),
                        Tab(text: "Tomorrow"),
                      ],
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Expanded(
                      child: TabBarView(
                        children: <Widget>[
                          SingleChildScrollView(
                            child: TodayCard(country.latlng),
                          ),
                          Container(
                            child: TomorrowCard(country.latlng),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
