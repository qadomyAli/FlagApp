import 'package:FlagApp/model/Country.dart';
import 'package:FlagApp/screens/MainPage.dart';
import 'package:FlagApp/screens/WeatherPage.dart';
import 'package:flutter/material.dart';

const String MainPageRoute = "/";
const String WeatherPageRoute = "weather";

Route<dynamic> onGenerateRoute(RouteSettings settings) {
  switch (settings.name) {
    case MainPageRoute:
      return MaterialPageRoute(builder: (BuildContext context) => MainPage());

    case WeatherPageRoute:
      return MaterialPageRoute(
          builder: (BuildContext context) => WeatherPage(
                country: Country(),
              ));

    default:
      return MaterialPageRoute(builder: (BuildContext context) => MainPage());
  }
}
