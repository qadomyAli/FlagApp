import 'package:FlagApp/model/Country.dart';
import 'package:FlagApp/screens/WeatherPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CountryCardWidget extends StatelessWidget {
  final Country country;

  CountryCardWidget({@required this.country});

  @override
  Widget build(BuildContext context) {
    if (country.numericCode == null) {
      country.numericCode = "Not Found";
    }
    return Container(
      child: Card(
        elevation: 0.0,
        child: Container(
          padding: EdgeInsets.all(5.0),
          child: Row(
            children: [
              FadeInImage.assetNetwork(
                width: 50.0,
                height: 50.0,
                // width: MediaQuery.of(context).size.width,
                placeholder: 'assets/images/no_data.png',
                image:
                    "http://www.geognos.com/api/en/countries/flag/${country.alpha2Code}.png",
                fit: BoxFit.fill,
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 25.0,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        country.name,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        country.numericCode,
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              FlatButton(
                onPressed: () {
                  // Navigator.of(context)
                  //     .pushNamed(WeatherPageRoute, arguments: country);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => WeatherPage(
                                country: country,
                              )));
                },
                child: Icon(
                  Icons.keyboard_arrow_right,
                  color: Colors.blue,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
