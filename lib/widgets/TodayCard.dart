import 'dart:convert';

import 'package:FlagApp/model/Weather.dart';
import 'package:FlagApp/screens/components/TextWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

class TodayCard extends StatefulWidget {
  TodayCard(this.latlng);

  final List<double> latlng;

  @override
  _TodayCardState createState() => _TodayCardState(latlng);
}

class _TodayCardState extends State<TodayCard> {
  final List<double> latlng;

  _TodayCardState(this.latlng);

  Weather weathers;

  var isLoading = false;

  Future _fetchData() async {
    setState(() {
      isLoading = true;
    });

    String url =
        "http://api.openweathermap.org/data/2.5/forecast?lat=${latlng[0]}&lon=${latlng[1]}&appid=1867722b6af87e1d0388e10c5a94be34";
    final response = await http.get(url);

    if (response.statusCode == 200) {
      weathers = Weather.fromJson(json.decode(response.body));
    }

    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    _fetchData();
  }

  @override
  Widget build(BuildContext context) {
    // if (weathers.list.isEmpty) {
    //   isLoading = false;
    // }

    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    final String date = formatter.format(weathers.list[0].dtTxt);

    return isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : Container(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.all(25),
                    child: Icon(
                      Icons.wb_sunny,
                      color: Colors.yellow,
                      size: 120,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(flex: 1, child: TextWidget("Date")),
                      Flexible(
                        flex: 2,
                        child: SizedBox(width: double.infinity),
                      ),
                      Flexible(flex: 1, child: Text(date))
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(flex: 1, child: TextWidget("Temp")),
                      Flexible(
                        flex: 2,
                        fit: FlexFit.tight,
                        child: SizedBox(width: double.infinity),
                      ),
                      Flexible(
                          flex: 1, child: Text("${weathers.list[0].main.temp}"))
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(flex: 1, child: TextWidget("Pressure")),
                      Flexible(
                        flex: 2,
                        child: SizedBox(width: double.infinity),
                      ),
                      Flexible(
                          flex: 1,
                          child: Text("${weathers.list[0].main.pressure}"))
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(flex: 1, child: TextWidget("Humidity")),
                      Flexible(
                        flex: 2,
                        child: SizedBox(width: double.infinity),
                      ),
                      Flexible(
                          flex: 1,
                          child: Text("${weathers.list[0].main.humidity}"))
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(flex: 1, child: TextWidget("Population")),
                      Flexible(
                        flex: 2,
                        child: SizedBox(width: double.infinity),
                      ),
                      Flexible(
                          flex: 1, child: Text("${weathers.city.population}"))
                    ],
                  ),
                ],
              ),
            ),
          );
  }
}
