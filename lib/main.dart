import 'package:FlagApp/providers/FlagAppProviders.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'Router.dart';

void main() {
  runApp(ChangeNotifierProvider(
    create: (BuildContext context) => FlagAppProviders(),
    child: MaterialApp(
      debugShowCheckedModeBanner: false,
      onGenerateRoute: onGenerateRoute,
      theme: ThemeData.light(),
      initialRoute: MainPageRoute,
    ),
  ));
}
